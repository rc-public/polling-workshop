const Redis = require('ioredis')

const argv = require('yargs').argv

const main = () => {
  const transaction = argv.transaction
  if (!transaction) throw new Error('--transaction [ID] missing')

  const delay = parseInt(argv.delay || 0)
  const prefix = argv.prefix || 'transaction'
  const status = argv.status || 'EXPIRED'

  const channel = `event:${prefix}:${transaction}`

  const pubsub = new Redis({
    port: 6379,
    host: 'localhost',
  });

  pubsub.on('connect', () => {
    const t = setTimeout(() => {
      clearTimeout(t)
        pubsub.publish(channel, status, () => {
          console.log('published', channel, status)
        })
        pubsub.disconnect()
    }, delay)
  })
}

main()