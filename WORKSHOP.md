### Tasks

- [ ] Create a pubsub service
- [ ] On PubsubService create a eventemitter and export de .once function
- [ ] On PSUBSCRIBE for trnasaction events end use the event emitter to handle the pmessages
- [ ] Use the receipt service to consume pubsub service and watch for transaction events
- [ ] Await for transaction completion to resolve the service function