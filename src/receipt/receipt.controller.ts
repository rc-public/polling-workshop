import { Controller, Get, Param } from '@nestjs/common';
import { ReceiptService } from './receipt.service'

@Controller('receipt')
export class ReceiptController {
  constructor(private readonly receiptService: ReceiptService) {}

  @Get('/:id')
  async getReceipt(@Param('id') id: string): Promise<string> {
    return await this.receiptService.awaitCompletion(id)
  }
}
