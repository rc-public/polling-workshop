import { Injectable } from '@nestjs/common';
import { CacheService } from '../cache/cache.service'

@Injectable()
export class ReceiptService {
  constructor(
    private readonly cacheService: CacheService,
  ){}

  private cachePrefix = 'transaction'

  private getTransactionEvent(transactionId) {
    return `event:${this.cachePrefix}:${transactionId}`
  }

  private getTransactionKey(transactionId){
    return `${this.cachePrefix}:${transactionId}`
  }

  async awaitCompletion(transactionId: string): Promise<any> {
    // get transaction fro cache

    // check transaction status, if !== CREATE or BOOKED, return the current status

    // await for transaction event 

    // update transaction status in cache

    //return the transaction status
    return transactionId
  }

}
