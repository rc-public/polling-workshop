import { Module } from '@nestjs/common';
import { ReceiptController } from './receipt.controller';
import { ReceiptService } from './receipt.service';
import { RedisModule } from 'nestjs-redis';
import { default as cacheConfig } from '../cache/cache.config';
import { CacheService } from '../cache/cache.service';

@Module({
  imports: [RedisModule.register(cacheConfig)],
  controllers: [ReceiptController],
  providers: [ReceiptService, CacheService]
})
export class ReceiptModule {}
