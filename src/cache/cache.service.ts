import { isNil } from 'lodash'
import { Redis } from 'ioredis'
import { Injectable } from '@nestjs/common'
import { RedisService } from 'nestjs-redis'

@Injectable()
export class CacheService {
  constructor(private readonly redisService: RedisService) {
    this.connect()
  }
  
  private _client: Redis

  connect(): void {
    this._client = this.redisService.getClient('cache')

    this._client.on('connect', () => {
      const { host, port } = this._client['options']
      console.log((`cache connected to ${host}:${port}`)) 
    })
    this._client.on('error', error =>{ console.log(`cache connection error: ${error.message}`) })
  }

  checkConnection(): any { 
    return this._client.status
  }

  async get(key: string): Promise<any> {
    if (isNil(key)) throw new Error('invalid key')
    
    return new Promise((resolve, reject) => {
      this._client.get(key, (error, response) => {
        if (error) {
          reject(error)
        } else if (!response) {
          resolve(null)
        }
        resolve(JSON.parse(response))
      })
    })
  }

  async set(key: string, value:object): Promise<any> {
    return new Promise((resolve, reject) => {
      this._client.set(key, JSON.stringify(value), (error) => {
        if (error) {
          reject(error)
        }
        resolve(value)
      })
    })
  }
}

